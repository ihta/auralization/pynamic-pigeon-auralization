from pynamic_pigeon_auralization import path_color
import numpy as np

#Saves plot in current working directory (default)

colorlist = path_color.get_color_list(
    comb_ord=3,
    direct_sound=np.array([255,255,255]),
    colormap='turbo',
    light_max=75,
    light_min=25,
    exp=0.5,
    saveplot=False,
    showplot=True,

)

print(colorlist)