from pathlib import Path

import bpy
from moviepy.editor import ImageSequenceClip
from blender_helpers import ensure_object_mode

# Combines blend_file and propagation paths (.blend files) from "visualizations" folder
# Creates .jpg files in "frames" folder and visualization.mp4

def render_animation(
    blend_file: Path,
    path_visualization_path: Path,
    working_dir: Path,
    *,
    export_debug_file=False,
):
    path_color = (1, 0.25, 0, 1)

    visualization_frames = list(path_visualization_path.glob("*.blend"))

    # Creates .jpg files for all frames
    for idx, visualization_frame in enumerate(visualization_frames):
        bpy.ops.wm.open_mainfile(filepath=str(blend_file))

        scene = bpy.context.scene
        start_frame = scene.frame_start

        ensure_object_mode()

        # Adds propagation paths of current frame to blend_file (described inside .ini file)
        bpy.ops.wm.append(
            filepath=str(visualization_frame / "Collection" / "PathVisualization"),
            directory=str(visualization_frame / "Collection"),
            filename="PathVisualization",
        )

        # Sets blend_file to correct frame and saves frame as image
        scene.frame_set(start_frame + idx)
        scene.render.filepath = str(working_dir / "frames" / f"{idx:04d}.jpg")
        bpy.ops.render.render(write_still=True)

        # In debug mode: Visualization_frame.blend contains propagation lines AND the blend_file properties
        if export_debug_file:
            bpy.ops.wm.save_as_mainfile(
                filepath=str(working_dir / "frames" / f"Visualization_frame.{idx:04d}.blend"),
                compress=False,
            )

    frames_dir = working_dir / "frames"
    frames = list(frames_dir.glob("*.png"))
    frames = [str(frame) for frame in frames]
    clip = ImageSequenceClip(frames, fps=bpy.context.scene.render.fps)
    clip.write_videofile(str(working_dir / "visualization.mp4"))
    clip.close()
