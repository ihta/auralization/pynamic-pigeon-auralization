import argparse
from pathlib import Path

import xarray as xr
from moviepy.editor import AudioFileClip, VideoFileClip

from pynamic_pigeon_auralization.plotting.get_hashed_ids import get_hashed_ids
from pynamic_pigeon_auralization.plotting.plot_prop_params import (
    animate_propagation_parameter,
    plot_propagation_parameter,
)
from pynamic_pigeon_auralization.plotting.read_prop_params import read_prop_parameters

propagation_parameter_abbreviations = {
    "spreading_loss": "SL",
    "diffraction_gain": "DG",
    "propagation_delay": "PD",
    "air_attenuation_third_octaves": "AA",
    "object_interaction_third_octaves": "OI",
}
inv_propagation_parameter_abbreviations = {v: k for k, v in propagation_parameter_abbreviations.items()}


def update_path_names(ds: xr.Dataset, matlab_ids: dict[str, str]) -> xr.Dataset:
    translated_names = []
    for name in ds.coords["paths"].values:
        matlab_id = name.replace("path_", "")
        if matlab_id in matlab_ids:
            translated_names.append(matlab_ids[matlab_id])
        else:
            translated_names.append(name)
    
    ds = ds.assign_coords(paths=translated_names)

    return ds


def main():
    parser = argparse.ArgumentParser(
        description="Read prop parameters and display them",
    )
    subparsers = parser.add_subparsers(title="subcommands", dest="subcommand", required=True)

    common_parser = argparse.ArgumentParser(add_help=False)
    common_parser.add_argument("output_path", type=Path, help="Path to a pynamic output directory")
    common_parser.add_argument(
        "--no-reuse", "-n", default=False, help="Do not reuse existing netcdf file", action="store_true"
    )
    common_parser.add_argument(
        "--sampling_rate",
        "-r",
        default=44100,
        type=int,
        help="The sampling rate of the auralization",
    )
    common_parser.add_argument(
        "--block_size",
        "-b",
        default=256,
        type=int,
        help="The block size of the auralization",
    )

    plot_parser = subparsers.add_parser(
        "plot", help="Create plots of the propagation parameters.", parents=[common_parser]
    )
    plot_parser.add_argument(
        "--propagation_parameters",
        "-p",
        nargs="+",
        help=(
            "The propagation parameters to plot, available parameters are: "
            f"{', '.join(f'{key} ({value})' for key, value in propagation_parameter_abbreviations.items())}"
        ),
        choices=[
            *propagation_parameter_abbreviations.keys(),
            *propagation_parameter_abbreviations.values(),
        ],
        metavar="PARAM",
    )
    plot_parser.add_argument(
        "--octave_plot",
        "-c",
        default="mean",
        help="Whether to plot the full octave bands or the mean",
        choices=["full", "mean"],
    )
    plot_parser.add_argument(
        "--show",
        "-s",
        default=False,
        action="store_true",
        help="Whether to show the plots",
    )
    plot_parser.add_argument(
        "--plot_output_path",
        "-o",
        type=Path,
        help="The path to save the plots. Relative paths are relative to the output path.",
        default="plots",
    )
    plot_parser.add_argument(
        "--plot_output_formats",
        "-f",
        nargs="+",
        default=["svg"],
        help="The formats to save the plots in, use 'fig' or 'pkl' to save the figure object for Python",
    )

    animate_parser = subparsers.add_parser(
        "animate", help="Create an animation of the propagation parameters.", parents=[common_parser]
    )
    animate_parser.add_argument(
        "--output_file",
        "-o",
        type=Path,
        help="The file name for the animation. Relative paths are relative to the output path.",
        default="prop_animation.mp4",
    )
    animate_parser.add_argument(
        "--no-audio",
        "-A",
        default=False,
        action="store_true",
        help="Do not include audio in the animation",
    )
    args = parser.parse_args()
    output_path = args.output_path
    no_reuse = args.no_reuse

    if not output_path.is_dir():
        msg = f"{output_path} is not a directory"
        raise ValueError(msg)

    if not (output_path / "auralization").is_dir():
        msg = f"{output_path} does not contain an auralization directory"
        raise ValueError(msg)

    if not (output_path / "paths").is_dir():
        msg = f"{output_path} does not contain an paths directory"
        raise ValueError(msg)

    if (output_path / "prop_params.nc").exists() and not no_reuse:
        print("Reading prop parameters from netcdf file")
        ds = xr.open_dataset(output_path / "prop_params.nc")
    else:
        ds = read_prop_parameters(output_path / "auralization")
        matlab_ids = get_hashed_ids(output_path / "paths")[1]
        ds = update_path_names(ds, matlab_ids)
        ds.to_netcdf(output_path / "prop_params.nc")

    if args.subcommand == "plot":
        if not args.plot_output_path.is_absolute():
            args.plot_output_path = output_path / args.plot_output_path

        propagation_parameters = None
        if args.propagation_parameters is not None:
            propagation_parameters = []
            for prop_param in args.propagation_parameters:
                if prop_param in inv_propagation_parameter_abbreviations:
                    propagation_parameters.append(inv_propagation_parameter_abbreviations[prop_param])
                else:
                    propagation_parameters.append(prop_param)

        plot_propagation_parameter(
            ds,
            propagation_parameters=propagation_parameters,
            octave_plot=args.octave_plot,
            show=args.show,
            plot_output_path=args.plot_output_path,
            plot_output_formats=args.plot_output_formats,
            sampling_rate=args.sampling_rate,
            block_size=args.block_size,
        )
    elif args.subcommand == "animate":
        if not args.output_file.is_absolute():
            args.output_file = output_path / args.output_file

        audio_file = None
        if (output_path / "auralization.wav").exists():
            audio_file = output_path / "auralization.wav"

        visualization_file = None
        fps = 30
        if (output_path / "visualization.mp4").exists():
            visualization_file = output_path / "visualization.mp4"

        animate_propagation_parameter(
            ds,
            output_file=args.output_file,
            sampling_rate=args.sampling_rate,
            block_size=args.block_size,
            frame_count=int(ds.coords["block"].size * args.block_size / args.sampling_rate * fps),
            audio_file=audio_file,
            visualization_file=visualization_file,
        )

        if not args.no_audio:
            input_video = VideoFileClip(str(args.output_file))
            input_audio = AudioFileClip(str(audio_file))
            input_video = input_video.set_audio(input_audio)

            temp_output_file = args.output_file.with_suffix(".audio.mp4")
            input_video.write_videofile(filename=str(temp_output_file), codec="libx264", audio_codec="aac")
            input_video.close()
            input_audio.close()

            args.output_file.unlink()
            temp_output_file.rename(args.output_file)


if __name__ == "__main__":
    main()
