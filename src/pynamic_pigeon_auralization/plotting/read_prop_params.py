import json
from pathlib import Path
from pprint import pprint

import numpy as np
import pandas as pd
import pyfar as pf
import xarray as xr


def read_prop_parameters(auralization_path: Path) -> xr.Dataset:

    # check that auralization_path is a directory and a path object
    if not isinstance(auralization_path, Path):
        auralization_path = Path(auralization_path)

    if not auralization_path.is_dir():
        msg = f"auralization_path must be a directory, not a file: {auralization_path}"
        raise ValueError(msg)

    json_files = list(auralization_path.glob("*.json"))

    prop_params_single_value: list[xr.DataArray] = []
    prop_params_third_oct: list[xr.DataArray] = []
    prop_params_vector: list[xr.DataArray] = []

    for json_file in json_files:
        df = pd.read_json(json_file)
        df.drop(columns=["sound_receiver_id", "sound_source_id"], inplace=True)
        df.index.name = "propagation_parameter"

        df = df.transpose()
        df.index.name = "paths"

        df_single_value = df[
            [
                "sound_source_id",
                "sound_receiver_id",
                "reflection_order",
                "diffraction_order",
                "spreading_loss",
                "diffraction_gain",
                "propagation_delay",
            ]
        ]

        df_single_value = df_single_value.astype("float64")
        prop_params_single_value.append(df_single_value.to_xarray().to_dataarray(dim="propagation_parameter"))

        df_third_oct = df[
            [
                "air_attenuation_third_octaves",
                "object_interaction_third_octaves",
            ]
        ]

        num_third_oct_bands = 31
        third_oct_raw_values = df_third_oct.to_numpy()
        third_oct_result_shape = (*third_oct_raw_values.shape, num_third_oct_bands)

        result_array = np.full(third_oct_result_shape, dtype=float, fill_value=np.nan)

        for i in range(third_oct_raw_values.shape[0]):
            for j in range(third_oct_raw_values.shape[1]):
                octave_values = third_oct_raw_values[i, j]
                arr = np.array(octave_values)
                result_array[i, j, : arr.size] = arr

        third_octave_freqs: np.array = pf.dsp.filter.fractional_octave_frequencies(num_fractions=3)[0]  # type: ignore
        third_octave_freqs = np.append(20, third_octave_freqs)

        da_third_oct = xr.DataArray(
            result_array,
            coords=[
                df_third_oct.index.values,
                df_third_oct.columns.values,
                third_octave_freqs,
            ],  # type: ignore
            dims=["paths", "propagation_parameter", "frequency"],
        )

        prop_params_third_oct.append(da_third_oct)

        df_vector = df[
            [
                "source_wavefront_normal",
                "receiver_wavefront_normal",
                "position",
            ]
        ]

        num_vec_dim = 3
        vec_raw_values = df_vector.to_numpy()
        vec_result_shape = (*vec_raw_values.shape, num_vec_dim)

        result_array = np.full(vec_result_shape, dtype=float, fill_value=np.nan)

        for i in range(vec_raw_values.shape[0]):
            for j in range(vec_raw_values.shape[1]):
                vec_values = vec_raw_values[i, j]
                arr = np.array(vec_values)
                result_array[i, j, : arr.size] = arr

        da_vector = xr.DataArray(
            result_array,
            coords=[
                df_vector.index.values,
                df_vector.columns.values,
                ["x", "y", "z"],
            ],  # type: ignore
            dims=["paths", "propagation_parameter", "vector"],
        )

        prop_params_vector.append(da_vector)

    ds_single_value = xr.concat(prop_params_single_value, dim="block").to_dataset(dim="propagation_parameter")
    ds_third_oct = xr.concat(prop_params_third_oct, dim="block").to_dataset(dim="propagation_parameter")
    ds_vector = xr.concat(prop_params_vector, dim="block").to_dataset(dim="propagation_parameter")

    ds = xr.merge([ds_single_value, ds_third_oct, ds_vector])

    return ds
