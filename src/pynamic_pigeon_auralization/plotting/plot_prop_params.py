import sys
from pathlib import Path
from typing import Dict, Literal, Tuple
import pickle

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pyfar as pf
import xarray as xr
from cycler import cycler
from moviepy.editor import VideoFileClip
from tqdm import tqdm


def plot_propagation_parameter(
    ds: xr.Dataset,
    propagation_parameters: str | list[str] | None = None,
    *,
    sampling_rate: int = 44100,
    block_size: int = 256,
    octave_plot: Literal["full", "mean"] = "full",
    show: bool = False,
    plot_output_path: Path | None = None,
    plot_output_formats: str | list[str] = "svg",
):
    if propagation_parameters is None:
        propagation_parameters = list(ds.data_vars.keys())

        propagation_parameters.remove("sound_source_id")
        propagation_parameters.remove("sound_receiver_id")
        propagation_parameters.remove("diffraction_order")
        propagation_parameters.remove("reflection_order")

        for key in (
            "source_wavefront_normal",
            "receiver_wavefront_normal",
            "position",
        ):
            propagation_parameters.remove(key)

    if isinstance(propagation_parameters, str):
        propagation_parameters = [propagation_parameters]

    if octave_plot not in ["full", "mean"]:
        msg = f"octave_plot must be either 'full' or 'mean', not {octave_plot}"
        raise ValueError(msg)

    if plot_output_path is not None and not isinstance(plot_output_path, Path):
        plot_output_path = Path(plot_output_path)

    if isinstance(plot_output_formats, str):
        plot_output_formats = [plot_output_formats]

    propagation_parameter_plots = _create_plots(
        ds,
        propagation_parameters,
        octave_plot,
        sampling_rate=sampling_rate,
        block_size=block_size,
    )

    for propagation_parameter, (fig, _) in propagation_parameter_plots.items():
        fig.set_size_inches(16, 8)

        if plot_output_path is not None:
            plot_output_path.mkdir(parents=True, exist_ok=True)

            for plot_output_format in plot_output_formats:
                plot_output_file = plot_output_path / f"{propagation_parameter}.{plot_output_format}"
                if plot_output_format in ["fig", "pkl"]:
                    pickle.dump(fig, open(plot_output_file, "wb"))
                else:
                    fig.savefig(plot_output_file, bbox_inches="tight")  # type: ignore

    if show:
        plt.show()


def animate_propagation_parameter(
    ds: xr.Dataset,
    *,
    sampling_rate: int = 44100,
    block_size: int = 256,
    frame_count: int = 30,
    output_file: Path | None = None,
    audio_file: Path | None = None,
    visualization_file: Path | None = None,
) -> None:
    from matplotlib import animation

    fig, axes = plt.subplot_mosaic(
        [
            ["spec", "spec", "spec", "video", "video"],
            ["param0", "param1", "param2", "param3", "param4"],
        ],
        layout="constrained",
        empty_sentinel="BLANK",
    )

    propagation_parameters = [
        "spreading_loss",
        "diffraction_gain",
        "propagation_delay",
        "air_attenuation_third_octaves",
        "object_interaction_third_octaves",
    ]

    desired_axes_order = [f"param{i}" for i in range(5)]

    lines = []

    for i, propagation_parameter in enumerate(propagation_parameters):
        da = ds[propagation_parameter]

        if "frequency" in da.coords:
            da = da.mean(dim="frequency")

        _, ax = _line_plot(
            da,
            propagation_parameter,
            sampling_rate=sampling_rate,
            block_size=block_size,
            ax=axes[desired_axes_order[i]],  # type: ignore
            add_block_axis=False,
        )

        lines.append(ax.axvline(x=0, color="b"))

    if audio_file is not None:
        audio = pf.io.read_audio(audio_file)
        audio = pf.dsp.normalize(audio)
        audio.fft_norm = "psd"
        ax, qm, _ = pf.plot.spectrogram(audio, ax=axes["spec"])
        ax[0].grid(linestyle=(0, (1, 10)))
        qm.set_rasterized(True)
        lines.append(ax[0].axvline(x=0, color="b"))

    video_clip = None
    if visualization_file is None:
        axes["video"].remove()
    else:
        video_clip = VideoFileClip(str(visualization_file))
        axes["video"].clear()
        axes["video"].imshow(video_clip.get_frame(0))
        axes["video"].axis("off")

    for title, ax in axes.items():
        if "param" in title:  # type: ignore
            ax.grid(visible=True, which="major")
            ax.sharex(axes["spec"])  # type: ignore

    fig.legend(ds.coords["paths"].values, loc="outside lower center", ncol=6)
    fig.subplots_adjust(left=0.05, wspace=0.3, hspace=0.35)
    fig.set_size_inches(16, 9)

    def animate(i):
        if video_clip:
            axes["video"].clear()
            axes["video"].imshow(video_clip.get_frame(i))
            axes["video"].axis("off")

        for line in lines:
            line.set_xdata([i, i])
        return tuple(lines)

    num_blocks = ds.coords["block"].size
    animation_duration = num_blocks * block_size / sampling_rate

    animation = animation.FuncAnimation(
        fig,
        animate,
        frames=tqdm(np.linspace(0, animation_duration, frame_count), desc="Animating", file=sys.stdout, leave=False),
        interval=animation_duration / frame_count,
    )

    animation.save(output_file, dpi=300, fps=frame_count / animation_duration)  # type: ignore


def _create_plots(
    ds: xr.Dataset,
    propagation_parameters: list[str],
    octave_plot: str,
    *,
    sampling_rate: int = 44100,
    block_size: int = 256,
) -> Dict[str, Tuple[plt.Figure, plt.Axes | np.ndarray[plt.Axes]]]:

    propagation_parameter_plots = {}

    for propagation_parameter in propagation_parameters:
        da = ds[propagation_parameter]

        if octave_plot == "mean" and "frequency" in da.coords:
            da = da.mean(dim="frequency")

        with pf.plot.context():
            if da.ndim == 2:  # noqa: PLR2004
                propagation_parameter_plots[propagation_parameter] = _line_plot(
                    da, propagation_parameter, sampling_rate=sampling_rate, block_size=block_size
                )

            elif da.ndim == 3 and "frequency" in da.coords:  # noqa: PLR2004
                propagation_parameter_plots[propagation_parameter] = _freq_plot(
                    da, propagation_parameter, sampling_rate=sampling_rate, block_size=block_size
                )

    return propagation_parameter_plots


def _line_plot(
    da: xr.DataArray,
    propagation_parameter: str,
    *,
    sampling_rate: int = 44100,
    block_size: int = 256,
    ax: plt.Axes | None = None,
    add_block_axis: bool = True,
) -> Tuple[plt.Figure | None, plt.Axes]:
    num_paths = len(da.coords["paths"])

    linestyles = ["-", "--", "-.", ":"]
    colors = mpl.color_sequences["tab10"]

    if num_paths > len(colors) * len(linestyles):
        colors = mpl.color_sequences["tab20"]

    custom_cycler = cycler(linestyle=linestyles) * cycler(color=colors)

    axis_given = True
    if ax is None:
        axis_given = False
        fig, ax = plt.subplots()

    ax.set_prop_cycle(custom_cycler)

    # todo correct the y-axis label depending on the propagation parameter
    pf_data = pf.TimeData(da.values.T, times=da.coords["block"] * block_size / sampling_rate)
    pf.plot.time(pf_data, ax=ax)
    ax.set_title(propagation_parameter)

    if propagation_parameter == "propagation_delay":
        ax.set_ylabel("Delay in s")

    if add_block_axis:
        ax2 = ax.twiny()
        ax2.set_xlabel("Block")
        ax2.set_xlim(0, da.coords["block"].size - 1)
        ax2.grid(False)
        ax2.grid(axis="x", which="major", linestyle=":")

    if axis_given:
        return None, ax

    fig.legend(da.coords["paths"].values, loc="outside lower center", ncol=6)

    return fig, ax


def _freq_plot(
    da: xr.DataArray,
    propagation_parameter: str,
    *,
    sampling_rate: int = 44100,
    block_size: int = 256,
) -> Tuple[plt.Figure, plt.Axes]:
    num_paths = len(da.coords["paths"])
    max_num_cols = 3
    num_cols = min(num_paths, max_num_cols)
    num_rows = int(np.ceil(num_paths / num_cols))

    fig, ax = plt.subplots(ncols=num_cols, nrows=num_rows, sharex=True, sharey=True, layout="constrained")

    cmap = mpl.colormaps.get_cmap("magma")
    cmap.set_bad(color="lightgray")

    for idx, path in enumerate(da.coords["paths"].values):
        curr_ax = ax.flat[idx]

        pf_data = pf.FrequencyData(da.sel(paths=path).values, frequencies=da.coords["frequency"])
        _, qm, _ = pf.plot.freq_2d(
            pf_data,
            ax=curr_ax,
            cmap=cmap,
            indices=da.coords["block"] * block_size / sampling_rate,
            colorbar=False,
        )
        curr_ax.set_title(path)
        curr_ax.set_xlabel("")
        curr_ax.set_ylabel("")

    # ax2 = ax.get_shared.twiny()
    # ax2.set_xlabel("Block")
    # ax2.set_xlim(0, da.coords["block"].size - 1)
    # ax2.grid(False)
    # ax2.grid(axis="x", which="major", linestyle=":")

    qm.set_clim(-60, 0)

    fig.suptitle(propagation_parameter)
    fig.supxlabel("Time [s]")
    fig.supylabel("Frequency [Hz]")
    fig.subplots_adjust(top=0.9, bottom=0.1, right=1, wspace=0.05, hspace=0.2)
    fig.colorbar(qm, ax=ax.ravel().tolist(), shrink=0.95, pad=0.05, label="Magnitude [dB]")

    return fig, ax
