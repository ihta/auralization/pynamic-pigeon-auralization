import hashlib
import json
from pathlib import Path
from typing import Dict, Tuple


def get_hashed_ids(path_list_path: Path) -> Tuple[Dict[str, str], Dict[str, str]]:

    if not isinstance(path_list_path, Path):
        path_list_path = Path(path_list_path)

    if not path_list_path.is_dir():
        msg = f"path_list_path must be a directory, not a file: {path_list_path}"
        raise ValueError(msg)

    json_files = list(path_list_path.glob("*.json"))

    identifiers = set()
    identifiers_matlab = set()

    for json_file in json_files:
        with open(json_file) as f:
            ppl = json.load(f)

            propagation_paths = ppl["propagation_paths"]

            for path in propagation_paths:
                matlab_id = []
                for i, anchor in enumerate(path["propagation_anchors"]):
                    if anchor["anchor_type"] in ("source", "emitter"):
                        matlab_id.append(f'S({anchor["name"]})')
                    elif anchor["anchor_type"] in ("receiver", "sensor"):
                        matlab_id.append(f'R({anchor["name"]})')
                    elif anchor["anchor_type"] == "specular_reflection":
                        matlab_id.append(f'SR({i}-{anchor["polygon_id"]})')
                    elif "diffraction" in anchor["anchor_type"]:
                        matlab_id.append(f'ED({i}-{anchor["main_wedge_face_id"]}-{anchor["opposite_wedge_face_id"]})')
                    else:
                        msg = f"Unknown anchor type: {anchor['anchor_type']}"
                        raise ValueError(msg)

                matlab_id = ":".join(matlab_id)

                identifiers_matlab.add(matlab_id)
                identifiers.add(path["identifier"])

    identifier_map = {}
    for identifier in identifiers:
        hashed_id = hashlib.md5(identifier.encode()).hexdigest()  # noqa: S324
        identifier_map[f"hash{hashed_id}"] = identifier

    identifier_matlab_map = {}
    for identifier in identifiers_matlab:
        hashed_id = hashlib.md5(identifier.encode()).hexdigest()  # noqa: S324
        identifier_matlab_map[f"hash{hashed_id}"] = identifier

    return identifier_map, identifier_matlab_map
