import bpy
import mathutils


def purge_orphans():
    """
    Taken from: https://github.com/CGArtPython/bpy_building_blocks_examples/blob/main/clean_scene/clean_scene_example_1.py
    """
    if bpy.app.version >= (3, 0, 0):
        bpy.ops.outliner.orphans_purge(do_local_ids=True, do_linked_ids=True, do_recursive=True)
    else:
        # call purge_orphans() recursively until there are no more orphan data blocks to purge
        result = bpy.ops.outliner.orphans_purge()
        if result.pop() != "CANCELLED":
            purge_orphans()


def clean_scene():
    """
    Taken from: https://github.com/CGArtPython/bpy_building_blocks_examples/blob/main/clean_scene/clean_scene_example_1.py
    Removing all of the objects, collection, materials, particles,
    textures, images, curves, meshes, actions, nodes, and worlds from the scene
    """
    if bpy.context.active_object and bpy.context.active_object.mode == "EDIT":
        bpy.ops.object.editmode_toggle()

    for obj in bpy.data.objects:
        obj.hide_set(False)
        obj.hide_select = False
        obj.hide_viewport = False

    bpy.ops.object.select_all(action="SELECT")
    bpy.ops.object.delete()

    # collection_names = [col.name for col in bpy.data.collections]
    # for name in collection_names:
    #     bpy.data.collections.remove(bpy.data.collections[name])

    # in the case when you modify the world shader
    # world_names = [world.name for world in bpy.data.worlds]
    # for name in world_names:
    #     bpy.data.worlds.remove(bpy.data.worlds[name])
    # # create a new world data block
    # bpy.ops.world.new()
    # bpy.context.scene.world = bpy.data.worlds["World"]

    purge_orphans()


def blender_2_open_gl(coord: [float]):
    return [coord[0], coord[2], -coord[1]]


def blender_2_quaternion(quat: [float]):
    return mathutils.Euler(quat).to_quaternion()[:]


def ensure_object_mode():
    """Ensure Blender is in Object Mode."""
    if bpy.context.object:
        if bpy.context.object.mode != 'OBJECT':
            bpy.ops.object.mode_set(mode='OBJECT')
