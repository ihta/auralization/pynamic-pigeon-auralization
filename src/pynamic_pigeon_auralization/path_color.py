import matplotlib.pyplot as plt
import numpy as np
import colorsys
import matplotlib.colors as mcolors
import matplotlib.patches as patches
from pathlib import Path
from matplotlib.ticker import MaxNLocator

#Convert colorvalue between RGB and HLS
def RGB_to_HLS(RGB):

    HLS = colorsys.rgb_to_hls(RGB[0]/255,RGB[1]/255,RGB[2]/255)*np.array([360 , 100 , 100])
    return HLS

def HLS_to_RGB(HLS):

    RGB = 255 * np.array(colorsys.hls_to_rgb(HLS[0]/360,HLS[1]/100,HLS[2]/100)) 
    return RGB    

#Create a Array with N RGB Colors from a Colormap
def get_N_colors_from_cmap(cmap, N):
    colors = [cmap(i) for i in np.linspace(0, 1, N)]
    rgb_colors = [mcolors.to_rgb(color) for color in colors]
    return rgb_colors

#'set_light_level_' creates lightning variations of Colors with bounderies Max and Min lightning (0-100) and exponential growth "exp"
# - 'Pure' only handels brightess variation of either pure Diffraction or pure Reflection within a single sound propagation line
# - 'Mixed' only handels brightness variation if Diffraction and Reflection happen both within a single sound propagation line
# - 'all' Handles both Pure and Mixed propagation line with a brightness variantion

def set_light_level_pure(max, min, new_rgb, exp, pathsegment_count, comb_ord): 
    new_hls = RGB_to_HLS(new_rgb)
    new_hls = np.array([new_hls[0], 0 , new_hls[2]])
    if comb_ord - 1 <= 0:
        add = [0,(max+min)/2,0] #No brightness variation needed for such small combined order
    else:
        add = [0,(max - (max-min)*((pathsegment_count-1)/(comb_ord-1))**exp),0]

    current_color = HLS_to_RGB( new_hls + add)
    return current_color

def set_light_level_mixed(max, min, new_rgb, exp, pathsegment_count, comb_ord): 
    new_hls = RGB_to_HLS(new_rgb)
    new_hls = np.array([new_hls[0], 0 , new_hls[2]])
    if comb_ord - 2 <= 0:
        add = [0,(max+min)/2,0] #No brightness variation needed for such small combined order
    else:
        add = [0,(max - (max-min)*((pathsegment_count-2)/(comb_ord-2))**exp),0]


    current_color = HLS_to_RGB( new_hls + add)

    return current_color

def set_light_level_all(max, min, new_rgb, exp, pathsegment_count, comb_ord): 
    new_hls = RGB_to_HLS(new_rgb)
    new_hls = np.array([new_hls[0], 0 , new_hls[2]])
    add = [0,(max - (max-min)*((pathsegment_count)/(comb_ord))**exp),0]
    current_color = HLS_to_RGB( new_hls + add)

    return current_color

# Function to define color of path depending on number of Diffractions and Reflections
def shift_method_purelightvariation(diff_count, refl_count, comb_ord, colors, direct_sound, light_max, light_min, exp):

    j = diff_count
    i = refl_count
    pathsegment_count = i + j

    max = light_max
    min = light_min 
    if i == 0 and j == 0:
        current_color = direct_sound

    elif i==0:

        new_rgb = colors[255]
        current_color = set_light_level_pure(max,min,new_rgb,exp,pathsegment_count,comb_ord)

    elif j==0:

        new_rgb = colors[0]
        current_color = set_light_level_pure(max,min,new_rgb,exp,pathsegment_count,comb_ord)

    else:
        new_index = int(30 +(255-30) * i/(pathsegment_count))
        new_rgb = colors[new_index]
        current_color = set_light_level_mixed(50,50,new_rgb,exp,pathsegment_count,comb_ord)

    return current_color

#Create a color Array with N elements
def create_colormap_array(colormap, array_size):
    
    cmap = plt.get_cmap(colormap) 
    N = array_size
    colors = np.array(get_N_colors_from_cmap(cmap, N)) *255
    colors = colors[::-1]#Inverted Colormap
    return colors


# Function assigns a unique color to all possible combinations of reflection and defraction
# returns following list: [number of reflections, number of diffractions, assigned color]
# Contrast of the lighting variations between colors can be set by changing light_max and light_min (0-100)
# exp changes the exponential growth of lighting variations between different path colors.
def get_color_list(comb_ord, direct_sound, colormap, light_max, light_min, exp, output_path: Path = Path.cwd(), saveplot=False, showplot=False):

    color_array = create_colormap_array(colormap,256)
    colorlist = []
    fig, ax = plt.subplots()
    ax.set_xlim(-0.5, comb_ord+1-0.5)
    ax.set_ylim(-0.5, comb_ord+1-0.5)
    plt.xlabel("Reflection")
    plt.ylabel("Diffraction")

    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))

    for i in range (comb_ord+1):
        refl_count = i

        for j in range (comb_ord+1):
            diff_count = j
        
            if diff_count+refl_count <= comb_ord:

                current_function = shift_method_purelightvariation
                current_color = current_function(diff_count, refl_count, comb_ord, color_array, direct_sound, light_max, light_min, exp)/255

                ax.add_patch(patches.Rectangle((i-0.5,j-0.5), 1, 1, facecolor = current_color))
                current_color = np.concatenate((current_color, [1])) #Addition 'alpha' A=1 for RGBA color code in blender
                colorlist.append((refl_count, diff_count, current_color))

    if saveplot:
        file_path = output_path / 'color_legend.png'
        plt.savefig(file_path)

    if showplot:
        plt.show()

    return colorlist
                        