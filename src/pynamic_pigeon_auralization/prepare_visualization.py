from pathlib import Path

import bpy
from blender_helpers import clean_scene
import re

# - Creates and adds a new Visualization_frame.blend file in the visualization folder
# - Visualization_frame.blend file only contains the sound propagation 
#   paths of the current frame (first audio block of frame)
# - All propagation paths have been assigned colors
def prepare_visualization(visualization: Path, output_path: Path, index: int, colorlist):
    clean_scene()

    bpy.ops.wm.collada_import(filepath=str(visualization))

    bpy.ops.object.select_all(action="DESELECT")

    # Delete all objects in visualization.dae that are not propagation paths
    bpy.data.objects["Scene"].select_set(True)
    bpy.context.view_layer.objects.active = bpy.data.objects["Scene"]
    bpy.ops.object.select_grouped(extend=True, type="CHILDREN_RECURSIVE")
    bpy.ops.object.delete()

    bpy.data.objects["Emitters"].select_set(True)
    bpy.ops.object.delete()

    bpy.data.objects["Sensors"].select_set(True)
    bpy.ops.object.delete()

    objects = bpy.data.objects

    # Set correct blender settings for all propagation paths
    for obj in objects:
        if obj is not None and obj.type == "MESH":

            # Make propagation paths visible in blender
            bpy.context.view_layer.objects.active = obj

            solidify = obj.modifiers.new("solidify", "SKIN")

            solidify.use_smooth_shade = True
            solidify.use_x_symmetry = True
            solidify.use_y_symmetry = True
            solidify.use_z_symmetry = True

            for v in obj.data.skin_vertices[0].data:
                v.radius = 0.05, 0.05

            subdiv = obj.modifiers.new("smooth", "SUBSURF")
            subdiv.render_levels = 1

            # Create list with all path objects and their reflection and diffraction number
            if bool(re.match(r'^E-E\d+.*S-S\d+$', obj.name)):
                
                # Count all diffractions (ED) and Reflections (SR) inside Path
                path_segment = list(re.findall(r'(ED|SR)', obj.name))
                diff_count = 0
                refl_count = 0

                for item in path_segment:
                    if item == "ED":
                        diff_count += 1
                    elif item == "SR":
                        refl_count += 1
  
                for item in colorlist:
                    if item[0] == refl_count and item[1] == diff_count:
                        new_color = item[2]

                # Add color correction sRGB to linear RGB for a correct color representation in Blender
                new_color_gamma_corrected=[]
                for c in new_color[:3]:
                    if   c < 0:       
                        new_color_gamma_corrected.append(0)
                    elif c < 0.04045: 
                        new_color_gamma_corrected.append(c/12.92)
                    else:             
                        new_color_gamma_corrected.append(((c+0.055)/1.055)**2.4)
                new_color_gamma_corrected.append(new_color[3])
                    
                # Add color to path     
                material = obj.data.materials[0]
                material.diffuse_color = new_color_gamma_corrected
                material.node_tree.nodes["Principled BSDF"].inputs["Base Color"].default_value = new_color_gamma_corrected
                material.node_tree.nodes["Principled BSDF"].inputs["Emission"].default_value = new_color_gamma_corrected        


    collection = bpy.data.collections.get("Collection")
    if collection:
        collection.name = "PathVisualization"

    visualization_dir = output_path / "visualizations"

    # Create "Visualizations" folder if it doesn't exist yet
    if not visualization_dir.exists():
        visualization_dir.mkdir()

    # Save new .blend file to folder
    bpy.ops.wm.save_as_mainfile(
        filepath=str(visualization_dir / f"Visualization_frame.{index:04d}.blend"),
        compress=False,
    )
