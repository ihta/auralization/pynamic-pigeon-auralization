import configparser
import math
import shutil
import tkinter as tk
import numpy as np
from datetime import datetime, timezone
from pathlib import Path
from tkinter import filedialog

import configargparse
import interpolate
import pyfar as pf
from moviepy.editor import AudioFileClip, CompositeVideoClip, VideoFileClip
from pigeon_project import PigeonProject
from prepare_visualization import prepare_visualization
from read_blend_file import read_blend_file
from render_animation import render_animation
from run_auralization import run_auralization
from spectrogram_animation import spectrogram_animation
from path_color import get_color_list


def main():
    parser = configargparse.ArgParser(
        prog="pigeon-anim",
        description="Generates a dynamic auralization with animation using pigeon",
        formatter_class=configargparse.ArgumentDefaultsHelpFormatter,
        default_config_files=["./animation_project.ini"],
    )
    parser.add_argument(
        "--config",
        is_config_file=True,
        help="Config file path for this app. When supplied, the config file path will be used as the base path",
        type=Path,
    )
    parser.add_argument("-p", "--pigeon", help="pigeon executable", type=Path)
    parser.add_argument(
        "-f",
        "--blend-file",
        help="blender file to animate",
        type=Path,
        dest="blend_file",
        required=True,
    )
    parser.add_argument(
        "-w",
        "--working-dir",
        help="Working directory",
        type=Path,
        default=Path(".")
        / "pigeon_{}".format(datetime.now(tz=timezone.utc).astimezone().strftime("%Y-%m-%dT%H%M%S")),
        dest="working_dir",
    )
    parser.add_argument("-d", "--debug", action="store_true")
    parser.add_argument(
        "-c",
        "--va-core-config",
        help="VACore config ini file used for auralization",
        type=Path,
        dest="va_core_config",
        required=True,
    )
    parser.add_argument(
        "-s",
        "--source-signal",
        help="Source signal used for the auralization",
        type=Path,
        dest="source_signal",
        required=True,
    )
    parser.add_argument(
        "-R",
        "--renderer-name",
        help="The name of the used renderer",
        type=str,
        default="MyBinauralOutdoorUrban",
        dest="renderer_name",
    )
    parser.add_argument(
        "-o",
        "--output",
        help="The output file, default is 'animation.mp4' inside the working dir",
        type=Path,
    )
    parser.add_argument(
        "-V",
        "--no-visualization",
        help="Disable visualization",
        action="store_true",
        dest="no_visualization",
    )
    parser.add_argument(
        "-P",
        "--no-path-simulation",
        help="Disable path simulation, also disables visualization",
        action="store_true",
        dest="no_path_simulation",
    )
    parser.add_argument(
        "-F",
        "--frames",
        help="Limit visual frames to process, zero indexed",
        nargs=2,
        type=int,
        metavar=("START-FRAME", "END-FRAME"),
    )
    parser.add_argument(
        "-C",
        "--no-combining",
        help=(
            "Disable combination of visualization and auralization, "
            "the combination only works if visualization and auralization are present"
        ),
        action="store_true",
        dest="no_combining",
    )
    parser.add_argument(
        "-A",
        "--no-auralization",
        help="Disable auralization",
        action="store_true",
        dest="no_auralization",
    )
    parser.add_argument(
        "-S",
        "--no-spectrogram",
        help="Disable spectrogram in the final animation",
        action="store_true",
        dest="no_spectrogram",
    )
    parser.add_argument(
        "-n",
        "--normalize",
        help="Normalize auralization",
        action="store_true",
    )
    parser.add_argument(
        "-ro",
        "--reflection-order",
        help="Reflection order to use",
        type=int,
        default=1,
        metavar="ORDER",
    )
    parser.add_argument(
        "-do",
        "--diffraction-order",
        help="Diffraction order to use",
        type=int,
        default=1,
        metavar="ORDER",
    )
    parser.add_argument(
        "-co",
        "--combined-order",
        help="Combined order to use",
        type=int,
        default=1,
        metavar="ORDER",
    )

    arguments = parser.parse_args()

    # Assignment of default working directory 
    if not arguments.output:
        arguments.output = arguments.working_dir / "animation.mp4"

    # Reading auralization configuration file (.ini) and adopting values
    if arguments.config:
        config_base = arguments.config.resolve().parent
        arguments.source_signal = config_base / arguments.source_signal
        arguments.va_core_config = config_base / arguments.va_core_config
        arguments.blend_file = config_base / arguments.blend_file

    # resolve() converts the Path to an absolute path, resolving any symlinks.
    arguments.working_dir = arguments.working_dir.resolve()
    arguments.output = arguments.output.resolve()
    arguments.source_signal = arguments.source_signal.resolve()
    arguments.va_core_config = arguments.va_core_config.resolve()
    arguments.blend_file = arguments.blend_file.resolve()

    print(arguments)

    # Reads Samplerate & Buffersize from va_config file
    config = configparser.ConfigParser()
    config.read(arguments.va_core_config)

    arguments.sampling_rate = config["Audio driver"].getint("Samplerate")
    arguments.block_size = config["Audio driver"].getint("Buffersize")

    # Locating the pigeon.exe file
    pigeon_path = arguments.pigeon
    if pigeon_path is None:
        # Search in PATH
        pigeon_path = shutil.which("pigeon.exe")

        # Lets user specify directory of pigeon.exe if nothing is found in PATH
        if pigeon_path is None:
            root = tk.Tk()
            root.withdraw()

            pigeon_path = filedialog.askopenfilename(
                title="Specify pigeon.exe",
                initialdir=".",
                filetypes=[("executable", "exe")],
            )

    # Creates two new directories within working_dir if they don't exist yet
    material_database_dir = arguments.working_dir / "material_database"
    if not material_database_dir.exists():
        material_database_dir.mkdir(parents=True)

    directivity_database_dir = arguments.working_dir / "directivity_database"
    if not directivity_database_dir.exists():
        directivity_database_dir.mkdir(parents=True)

    # Reads blender file to:
    #   - extract information about location and rotation of source and receiver for every single frame
    #   - extract fps of blender file
    #   - create geometry.dae file with only static information about original blender file
    animation_data, fps, geometry_path = read_blend_file(arguments.blend_file, arguments.working_dir)

    # Deleting all animation data outside of needed frames 
    # (Only if a Limit of visual frames to process is declared by parser)
    if arguments.frames:
        animation_data.source_loc = animation_data.source_loc[arguments.frames[0] : arguments.frames[1]]
        animation_data.source_rot = animation_data.source_rot[arguments.frames[0] : arguments.frames[1]]
        animation_data.receiver_loc = animation_data.receiver_loc[arguments.frames[0] : arguments.frames[1]]
        animation_data.receiver_rot = animation_data.receiver_rot[arguments.frames[0] : arguments.frames[1]]

    # Creation and initialisation of object proj with current directories and refl, diff & comb orders
    # Later proj.run is used for running pigeon.exe
    proj = PigeonProject(
        pigeon_path,
        geometry_path,
        arguments.working_dir,
        reflection_order=arguments.reflection_order,
        diffraction_order=arguments.diffraction_order,
        combined_order=arguments.combined_order,
    )

    # Definition audio block:   Chunk of audio samples being processed simultaneously.
    #                           Amount of samples per audio block is calculated using 
    #                           buffersize & sample rate

    # Calculating total amount of audio blocks in project and audio blocks per frame
    animation_frames = len(animation_data.source_loc)
    animation_length_sec = animation_frames / fps
    auralization_delta_t = arguments.block_size / arguments.sampling_rate
    num_audio_blocks = math.ceil(animation_length_sec / auralization_delta_t)
    audio_blocks_per_frame = num_audio_blocks / animation_frames

    # Function for interpolating a 3D curve (for source and receiver). 
    # Returns a finer set of interpolated points
    interpolated_source_loc = interpolate.interpolate_3d_curve(animation_data.source_loc, num_audio_blocks)
    interpolated_receiver_loc = interpolate.interpolate_3d_curve(animation_data.receiver_loc, num_audio_blocks)

    # Function interpolates a list of rotations represented as quaternions 
    # Returns a finer set of interpolated rotations 
    interpolated_source_rot = interpolate.interpolate_rotation(animation_data.source_rot, num_audio_blocks)
    interpolated_receiver_rot = interpolate.interpolate_rotation(animation_data.receiver_rot, num_audio_blocks)

    if not arguments.no_path_simulation:
        frame_idx = 0
        
        # Function assigns a unique color to all possible combinations of reflection and diffraction
        # Returns following list: [number of reflections, number of diffractions, assigned colors]
        # See path_color.py for more infos 
        colorlist = get_color_list(
            comb_ord=arguments.combined_order,
            direct_sound=np.array([255,255,255]),
            colormap='turbo',
            light_max=75,
            light_min=25,
            exp=0.5,
            output_path=proj.working_dir,
            saveplot=True,
        )

        # Operations carried out for each audio block. 
        # One visual frame contains several audio blocks (audio_blocks_per_frame)
        for block_idx in range(num_audio_blocks):
            visual_frame = False

            # Only one visualization needed per frame
            if (block_idx % audio_blocks_per_frame) < 1 and not arguments.no_visualization:
                visual_frame = True

            proj.export_visualization = visual_frame

            # Calls pigeon.exe
            # Creates & updates: visualization.dae, pigeon_config.ini, pigeon_result.json, pigeon_config_stats.json
            # Returns path of visualization.dae
            visualization = proj.run(
                source=interpolated_source_loc[block_idx],
                receiver=interpolated_receiver_loc[block_idx],
                source_rot=interpolated_source_rot[block_idx],
                receiver_rot=interpolated_receiver_rot[block_idx],
            )

            path_list = proj.working_dir / proj.result_file

            # Creates "paths" folder in working directory if not existing
            paths_dir = arguments.working_dir / "paths"
            if not paths_dir.exists():
                paths_dir.mkdir()

            # Copies (for current audio block) a renamed and numbered version of pigeon_result to "paths" folder
            shutil.copy(
                path_list,
                arguments.working_dir / "paths" / f"block.{block_idx:04d}.json",
            )

            # Creates visualization of sound propagation paths using current visualization.dae
            # - Creates "visualizations" folder once
            # - Creates coloured propagation paths "visualization_frame.blend" inside folder for current frame
            if visual_frame:
                prepare_visualization(
                    visualization=visualization,
                    output_path=proj.working_dir,
                    index=frame_idx,
                    colorlist=colorlist, #adds colors to distinguish between diffraction, reflection and direct sound
                )
                frame_idx = frame_idx + 1

    # Run MATLAB auralization file "run_auralization.m"
    # Creates: auralization.wav, auralization_spec.mp4; 
    if not arguments.no_auralization:
        run_auralization(
            path_list_dir=arguments.working_dir / "paths",
            working_dir=arguments.working_dir,
            combined_order=proj.combined_order,
            va_core_config=arguments.va_core_config,
            sampling_rate=arguments.sampling_rate,
            block_size=arguments.block_size,
            source_signal=arguments.source_signal,
            renderer_name=arguments.renderer_name,
        )

    # Normalize audio
    if arguments.normalize:
        auralization = pf.io.read_audio(arguments.working_dir / "auralization.wav")
        auralization = pf.dsp.normalize(auralization)
        pf.io.write_audio(auralization, arguments.working_dir / "auralization.wav")

    # Combines blend_file with coloured propagation paths
    # Creates visualization.mp4 and .jpg files in "Frames" Folder
    if not arguments.no_visualization:
        render_animation(
            blend_file=arguments.blend_file,
            path_visualization_path=arguments.working_dir / "visualizations",
            working_dir=arguments.working_dir,
            export_debug_file=arguments.debug,
        )

    # Combine visualization and auralization and create "animation.mp4" (if not renamed with parser)
    if not arguments.no_combining:
        input_video = VideoFileClip(str(arguments.working_dir / "visualization.mp4"))

        input_audio = AudioFileClip(str(arguments.working_dir / "auralization.wav"))


        if not arguments.no_spectrogram:
            # Create Spectogram "auralization_spec.mp4" 
            spectrogram_animation(
                arguments.working_dir / "auralization.wav",
                arguments.working_dir / "auralization_spec.mp4",
                animation_frames,
            )

            # Add spectogram to animation.mp4    
            spectrogram_overlay = VideoFileClip(str(arguments.working_dir / "auralization_spec.mp4"))

            spectrogram_overlay = spectrogram_overlay.resize(newsize=0.25)

            input_video = CompositeVideoClip([input_video, spectrogram_overlay.set_position(("left", "bottom"))])

        # Add audio to video
        input_video = input_video.set_audio(input_audio)

        # Write final video file
        input_video.write_videofile(filename=str(arguments.output), codec="libx264", audio_codec="aac")

        input_video.close()
        input_audio.close()

if __name__ == "__main__":
    main()
