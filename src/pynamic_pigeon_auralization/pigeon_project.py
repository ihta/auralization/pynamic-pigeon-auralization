import configparser
import subprocess
from pathlib import Path


class PigeonProject:
    export_visualization = True
    visualization_file = Path("visualization.dae")
    result_file = Path("pigeon_result.json")
    config_file = Path("pigeon_config.ini")
    material_database = Path("material_database")
    directivity_database = Path("directivity_database")
    geometry_file: Path

    def __init__(
        self,
        pigeon_exe: Path,
        geometry_file: Path,
        working_dir: Path,
        reflection_order: int,
        diffraction_order: int,
        combined_order: int,
    ) -> None:
        self.pigeon_exe = pigeon_exe
        self.geometry_file = geometry_file
        self.working_dir = working_dir
        self.reflection_order = reflection_order
        self.diffraction_order = diffraction_order
        self.combined_order = combined_order

    # Function to write a config file for pigeon_exe 
    # Returns directory of created config file
    def create_config(self, source, receiver, source_rot=None, receiver_rot=None):
        config = configparser.ConfigParser()

        config["pigeon:scene"] = {}
        section = config["pigeon:scene"]

        section["GeometryFilePath"] = str(self.geometry_file)
        section["OutputFilePath"] = str(self.working_dir / self.result_file)
        section["MaterialDatabase"] = str(self.working_dir / self.material_database)
        section["DirectivityDatabase"] = str(self.working_dir / self.directivity_database)

        config["pigeon:scene:emitters"] = {}
        section = config["pigeon:scene:emitters"]
        section["Emitter1"] = ",".join(map(str, ["E1", *source, *source_rot, "directivity"]))

        config["pigeon:scene:sensors"] = {}
        section = config["pigeon:scene:sensors"]
        section["Sensor1"] = ",".join(map(str, ["S1", *receiver, *receiver_rot, "directivity"]))

        config["pigeon:config"] = {}
        section = config["pigeon:config"]

        section["MaxDiffractionOrder"] = str(self.diffraction_order)
        section["MaxReflectionOrder"] = str(self.reflection_order)
        section["MaxCombinedOrder"] = str(self.combined_order)
        section["ExportVisualization"] = str(self.export_visualization)

        section["FilterNotVisiblePaths"] = str(True)

        config["pigeon:visualization"] = {}
        section = config["pigeon:visualization"]

        section["VisualizationFilePath"] = str(self.working_dir / self.visualization_file)
        section["VisualizationFileFormat"] = "collada"

        with open(self.working_dir / self.config_file, "w") as config_file:
            config.write(config_file)

        return self.working_dir / self.config_file

    # Creates config file with given parameters and executes pigeon_exe with created config_file
    # Execution of pigeon_exe creates/updates:  visualization.dae, pigeon_result.json, pigeon_config_stats.json
    def run(self, source, receiver, source_rot=None, receiver_rot=None):
        config_file = self.create_config(
            source=source,
            receiver=receiver,
            source_rot=source_rot,
            receiver_rot=receiver_rot,
        )

        subprocess.run([self.pigeon_exe, config_file], check=True, cwd=".")

        return self.working_dir / self.visualization_file
