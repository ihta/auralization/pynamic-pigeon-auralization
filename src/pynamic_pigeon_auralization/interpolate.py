import numpy as np
from scipy import interpolate
from scipy.spatial.transform import Rotation as Rot
from scipy.spatial.transform import Slerp

# Function for interpolating a 3D curve using a given set of points
# returns a finer set of interpolated points.
def interpolate_3d_curve(points: [[float]], num_sampling_points: int):
    samples = np.array(points)

    t = np.linspace(0, 1, len(points))
    bspl = interpolate.make_interp_spline(t, samples, k=5, axis=0)

    tt = np.linspace(0, 1, num_sampling_points)

    fine_points = bspl(tt)

    return fine_points.tolist()

# Function takes a list of rotations represented as quaternions
# returns a finer set of interpolated rotations using spherical linear interpolation (slerp).
def interpolate_rotation(rotations: [[float]], num_sampling_points: int):
    rotations = np.array(rotations)
    rotations = rotations[:, [1, 2, 3, 0]]

    samples = Rot.from_quat(rotations)

    t = np.linspace(0, 1, len(rotations))
    slerp = Slerp(t, samples)

    tt = np.linspace(0, 1, num_sampling_points)
    fine_points = slerp(tt)

    return fine_points.as_quat()
