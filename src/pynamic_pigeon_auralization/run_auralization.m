%% checks
assert(exist('itaAudio', 'class'), 'Cannot find itaAudio class. Did you add the ITA toolbox folder to the Matlab path?')
assert(exist('VA', 'class'), 'Cannot find VA class. Did you add the VAMatlab folder to the Matlab path?')

va_version = split(VA.get_version(), [" ", "."]);
assert(str2double(va_version{2}) >= 2023, 'VAMatlab version is lower than 2023. please use a newer version')

if ~exist('working_dir','var')
    % script run from standalonme Matlab; add reasonable default values
    % this is done to be able to run the auralization separately
    working_dir='./pigeon_tmp';
    path_list_dir=[working_dir filesep 'paths'];
    auralization_dir=[working_dir filesep 'auralization'];
    combined_order=2;
    va_core_config='change-me.ini';
    renderer_name='MyBinauralOutdoorUrban';
    sampling_rate=44100;
    block_size=256;
    source_signal='change-me.wav';
end

%% settings
freq_vector = ita_ANSI_center_frequencies;

%% get path list jsons
path_list_files = dir([path_list_dir filesep '*.json']);

%% init propagation engine
propagation = itaGeoPropagation;
propagation.freq_vector = freq_vector.';
propagation.sim_prop.orders.combined = combined_order;

%% calculate auralization parameters
old_urban_paths = [];

% TODO, this has to be set dynamically at some point
S = 1;
R = 1;

for idx_frame = 1:numel(path_list_files)
    urbanPaths = ita_propagation_load_paths( [path_list_files(idx_frame).folder filesep path_list_files(idx_frame).name ] );
    
    if ~isempty(urbanPaths)

        [spreading_loss, diffraction_gain, delay, air_attenuation, object_interaction, ...
            source_wf_normal, receiver_wf_normal, valid, ~] = ...
            propagation.wavefront_coeffs(urbanPaths);
        
        [urbanPaths, hashTable] = ita_propagation_paths_add_identifiers(urbanPaths);

        paths_update = ita_propagation_va_struct(urbanPaths,...
            old_urban_paths,...
            S,...
            R,...
            spreading_loss,...
            diffraction_gain,...
            delay,...
            source_wf_normal,...
            receiver_wf_normal,...
            air_attenuation,...
            object_interaction,...
            [], false);
    else
        warning(['Frame ' num2str(idx_frame) ' has no urban paths'])

        paths_update = struct;
        paths_update.sound_source_id = S;
        paths_update.sound_receiver_id = R;
    end
    
    old_urban_paths = urbanPaths;
    
    % save to file
    json_encoded = jsonencode(paths_update,'PrettyPrint', true);
    path_update_file_name = sprintf('path_update_block.%04d.json', idx_frame - 1);
    
    if ~exist(auralization_dir, 'dir')
       mkdir(auralization_dir)
    end
    
    fid = fopen([auralization_dir filesep path_update_file_name],'w');
    fprintf(fid,'%s',json_encoded);
    fclose(fid);
%     save( auralization_frame_files{idFrame}, 'paths_update', 'hashTable' ); % save as json?
end

%% auralization settings
output_filename = 'auralization.wav';
sound_power = 20e-3;

deltaT = block_size/sampling_rate;

%% prepare VA
va = VA();

va_server_exe = which('VAServer.exe');
if isempty(va_server_exe)
    [ basename, basepath ] = uigetfile('VAServer.exe','Select VAServer executable','VAServer');
    va_server_exe = [ basepath filesep basename ];
end

assert(isfile(va_server_exe), 'Invalid path to VAServer.exe.')

va_server_path = fileparts(va_server_exe);

va_base_path = fullfile( va_server_path, '..' );

va_args = [ 'localhost:12340 "' va_core_config '" -r &'];
os_call = [ va_server_exe ' ' va_args ];
% ... starts in remote control mode, use va.shutdown_server to stop server and export WAV file
system(os_call);

%% initialize VA scene
va.connect( 'localhost' );

va.set_output_gain( 0.5 )

% tmp_filename = [working_dir filesep 'pigeon_auralization_tmp.wav'];
tmp_filename = 'pigeon_auralization_tmp.wav';
disp(tmp_filename)
va.set_rendering_module_parameters( renderer_name, struct('RecordOutputFileName', tmp_filename) );
va.set_rendering_module_parameters( renderer_name, struct('RecordOutputBaseFolder', working_dir) );

va.add_search_path( fullfile(va_base_path,'data') );
va.add_search_path( cd );
va.add_search_path( working_dir );

%Signal source
signal_source = va.create_signal_source_buffer_from_file( source_signal );
va.set_signal_source_buffer_looping( signal_source, true );
va.set_signal_source_buffer_playback_action( signal_source, 'play' )

%Sound source
sound_source = va.create_sound_source( 'itaVA_Source' );
va.set_sound_source_signal_source( sound_source, signal_source );
va.set_sound_source_sound_power( sound_source, sound_power);
va.set_sound_source_position(sound_source, [0 0 2]);
va.set_sound_source_orientation_view_up(sound_source, [0 0 -1],[0 1 0]);

%Receiver
sound_receiver = va.create_sound_receiver( 'itaVA_Listener' );
va.set_sound_receiver_position( sound_receiver, [0 0 -2] )
va.set_sound_receiver_orientation_view_up( sound_receiver, [-1 0 0],[0 1 0]);
HRIR = va.create_directivity_from_file( '$(DefaultHRIR)' );
va.set_sound_receiver_directivity( sound_receiver, HRIR );

%% Preprocessing so that VDL can fill up
manual_clock = 0;
va.set_core_clock( 0 );
t_pre_proc = 2;
n_pre_blocks = ceil( t_pre_proc * sampling_rate / block_size );
n_pre_samples = n_pre_blocks*block_size;

for idFrame = 1:n_pre_blocks
    va.call_module( 'virtualaudiodevice', struct( 'trigger', true ) );
    
    manual_clock = manual_clock + deltaT;
    va.call_module( 'manualclock', struct( 'time', manual_clock ) );
end

%% Run actual auralization
path_update_files = dir([auralization_dir filesep '*.json']);

for idx_frame = 1:numel(path_update_files)
    
    path_update_file = [path_update_files(idx_frame).folder filesep path_update_files(idx_frame).name ];
    
    json_data = fileread(path_update_file);
    paths_update = jsondecode(json_data);
    
    
    % Make source_pos, receiver_pos and paths_update available for this
    % frame
%     load( auralization_frame_files{idFrame} , 'paths_update', '-mat' );
%     path_names = fieldnames(paths_update);
          
    % Update all propagation paths
%     source_pos_OpenGL = ita_matlab2openGL( carPositions(idFrame, :) );
%     receiver_pos_OpenGL = ita_matlab2openGL(receiverPosition);
%     
%     va.set_sound_source_position( S, source_pos_OpenGL );
%     va.set_sound_receiver_position( R, receiver_pos_OpenGL );
    
    va.set_rendering_module_parameters( renderer_name, paths_update );
    
    % Process audio chain by incrementing one block
    va.call_module( 'virtualaudiodevice', struct( 'trigger', true ) );
       
    manual_clock = manual_clock + deltaT;
    va.call_module( 'manualclock', struct( 'time', manual_clock ) );
end

pause(1)
va.shutdown_server();
pause(5); %Wait for VA to write file
va.disconnect();

%% Remove time used to fill up VDL
audio_samples = audioread([working_dir filesep tmp_filename]);
audio_samples = audio_samples(n_pre_samples+1:end, :);
audiowrite([working_dir filesep output_filename], audio_samples, sampling_rate, 'BitsPerSample', 32);
