import sys
from pathlib import Path

import bpy
from blender_helpers import blender_2_open_gl, blender_2_quaternion, ensure_object_mode



class DotDict(dict):
    """dot.notation access to dictionary attributes"""

    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

# Reads blender file (defined in .ini File and located in "geometries" Folder) to extract data.
# Returns following:
#   animation_data: Contains information about location and rotation of source and receiver object
#                   for every frame of the scene
#   fps:            'Frames per second' of scene
#   geometry_path:  creates geometry.dae file which contains only static geometric data of original 
#                   blender file 


def read_blend_file(blend_file: Path, working_dir: Path):
    bpy.ops.wm.open_mainfile(filepath=str(blend_file))

    ensure_object_mode()

    objects = bpy.data.objects

    if "Source" not in objects:
        print("Source object not found in blend file")
        sys.exit()

    if "Receiver" not in objects:
        print("Receiver object not found in blend file")
        sys.exit()

    if "Geometry" not in objects:
        print("Geometry object not found in blend file")
        sys.exit()

    source = objects["Source"]
    receiver = objects["Receiver"]
    geometry = objects["Geometry"]

    # Bake source motion
    bpy.ops.object.select_all(action="DESELECT")
    source.select_set(True)
    bpy.context.view_layer.objects.active = source
    bpy.ops.nla.bake(visual_keying=True, clear_constraints=True, bake_types={"OBJECT"})

    # Bake receiver motion
    bpy.ops.object.select_all(action="DESELECT")
    receiver.select_set(True)
    bpy.context.view_layer.objects.active = receiver
    bpy.ops.nla.bake(visual_keying=True, clear_constraints=True, bake_types={"OBJECT"})

    scene = bpy.context.scene

    start_frame = scene.frame_start
    end_frame = scene.frame_end
    num_frames = end_frame - start_frame + 1

    animation_data = DotDict(
        {
            "source_loc": [None] * num_frames,
            "source_rot": [None] * num_frames,
            "receiver_loc": [None] * num_frames,
            "receiver_rot": [None] * num_frames,
        }
    )

    # Extract and convert data from blender scene to animation_data for every frame
    # Data contains information about location and rotation of source and receiver
    for idx, frame in enumerate(range(start_frame, end_frame + 1)):
        scene.frame_set(frame)

        animation_data.source_loc[idx] = blender_2_open_gl(source.location[:])
        animation_data.receiver_loc[idx] = blender_2_open_gl(receiver.location[:])

        if source.rotation_mode == "QUATERNION":
            animation_data.source_rot[idx] = source.rotation_quaternion[:]
        else:
            animation_data.source_rot[idx] = blender_2_quaternion(source.rotation_euler[:])

        if receiver.rotation_mode == "QUATERNION":
            animation_data.receiver_rot[idx] = receiver.rotation_quaternion[:]
        else:
            animation_data.receiver_rot[idx] = blender_2_quaternion(receiver.rotation_euler[:])

    # Creates file with only static geometric data of original blender file 
    # without information about source and receiver
    bpy.ops.object.select_all(action="DESELECT")
    geometry.select_set(True)
    bpy.context.view_layer.objects.active = geometry

    geometry_path = working_dir / "geometry.dae"
    bpy.ops.wm.collada_export(filepath=str(geometry_path), selected=True, triangulate=False)

    return animation_data, bpy.context.scene.render.fps, geometry_path

