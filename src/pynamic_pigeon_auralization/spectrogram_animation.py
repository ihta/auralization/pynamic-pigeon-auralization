from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pyfar as pf
from matplotlib import animation


def spectrogram_animation(auralization: Path, output: Path, num_frames: int):
    fig, ax = plt.subplots()

    audio = pf.io.read_audio(auralization)
    pf.plot.spectrogram(audio, ax=ax)

    ax.grid(False)

    line = ax.axvline(x=0, color="w", label="axvline - full height")

    def animate(i):
        line.set_xdata([i, i])
        return (line,)

    ani = animation.FuncAnimation(
        fig,
        animate,
        frames=np.linspace(0, audio.signal_length, num_frames),
        interval=audio.signal_length / num_frames * 1000,
    )

    ani.save(output, dpi=300)
