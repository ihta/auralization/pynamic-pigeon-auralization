import shutil
import subprocess
import sys
from pathlib import Path


def run_auralization(
    path_list_dir: Path,
    working_dir: Path,
    combined_order: int,
    va_core_config: Path,
    sampling_rate: int,
    block_size: int,
    source_signal: Path,
    renderer_name: str = "MyBinauralOutdoorUrban",
):
    # check for matlab in Path
    if shutil.which("matlab") is None and sys.platform == "win32":
        msg = "Could not find matlab exe in path."
        raise RuntimeError(msg)

    # Todo: maybe adapt core config to explicitly be for offline rendering.
    # i.e. change audio device sampling rate etc
    # Todo: maybe add additional data path, maybe easier with 2023b

    script_dir = Path(__file__).parent.resolve()
    matlab_script = script_dir / "run_auralization.m"

    # Creates and adds auralization.wav to working directory
    subprocess.run(
        [
            "matlab",
            "-batch",
            """
            working_dir='{}';
            path_list_dir='{}';
            auralization_dir='{}';
            combined_order={};
            va_core_config='{}';
            renderer_name='{}';
            sampling_rate={};
            block_size={};
            source_signal='{}';
            run('{}');
            quit
            """.format(
                working_dir,
                path_list_dir,
                working_dir / "auralization",
                combined_order,
                va_core_config,
                renderer_name,
                sampling_rate,
                block_size,
                source_signal,
                matlab_script,
            ),
        ],
        check=True,
        cwd=".",
    )
