# SPDX-FileCopyrightText: 2023-present Pascal Palenda <ppa@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0
__version__ = "0.0.1"
