# Example auralizations

In this directory, configurations for some example auralizations are stored.
The `ini` files use the following naming scheme

> `{geometry}-{spatialization}-{source signal}-co{combined order}.ini`

In the folders `geometries`, `source-signals` and `va-configs` the corresponding files can be found.

The following example geometries are included:

## wedge

This is a simple 2D, 90° wedge with the source rotating around the apex.

![img](geometries/wedge.png){width=75%}

## step

This example uses a 2D step, used in Stienen, "Real-Time Auralisation of Outdoor Sound Propagation", Fig 3.9.
The source is rotating around the step's apex edge.

![img](geometries/step.png){width=75%}

## ground

Just a ground plane with a simple car pass-by animation.

![img](geometries/ground.png){width=75%}

## cube

A cuboid building on a ground plane.
The source is rotating around the building.

![img](geometries/cube.png){width=75%}

## test_scene

A more complex 3D scene with a car pass-by animation.

![img](geometries/test_scene.png){width=75%}
