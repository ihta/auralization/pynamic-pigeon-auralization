# HOW TO CONTRIBUTE

## Guidelines

- Write meaningful commit messages.
  For example follow [these guidelines](https://ec.europa.eu/component-library/v1.15.0/eu/docs/conventions/git/).
- Prefer small commits over commits with many changes.
  When using the command line interface of git, take a look at [`git add --patch`](https://levelup.gitconnected.com/staging-commits-with-git-add-patch-1eb18849aedb).
- For each new feature create a feature branch in the form of `feature/YOUR-FEATURE` (Without the capitalization).
  Afterwards, create a merge request for it.
- Before committing changes to the Python code: always run the linting and formatting (`hatch run lint:fmt`).
