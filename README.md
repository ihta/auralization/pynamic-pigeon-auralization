# Pynamic-Pigeon-Auralization

`python` scripts to generate a dynamic auralization and animation of a 3D scene based on propagation path simulations from [pigeon](https://www.virtualacoustics.de/GA/iem/#pigeon).
The scene and animations are defined and rendered using __Blender__ and its `python` API.
Furthermore, the resulting auralization parameters can be visualized using [`pynamic-plot`](#plotting-the-auralization-parameters).

## General requirements

In order for these scripts to work some requirements must be met.

1. **Matlab environment**

   A recent version of Matlab has to be installed.
   It is recommended to have a version higher than 2019a.

   Furthermore, the [ITA-Toolbox](https://git.rwth-aachen.de/ita/toolbox) and [VAMatlab](https://www.virtualacoustics.de/VA/download/) must be installed.

2. **Pigeon**

   A copy of [pigeon](https://www.virtualacoustics.de/GA/iem/#pigeon) version above 2021a (!) must be available as well.

   Note, that at the time of writing this is not available as a binary release and must be built from source.

## Installation

### For users

A Python environment compatible with 3.10[^1] must be installed.
It is recommended to use a virtual environment for this.

```bash
py -m venv .env
```

or

```bash
py -3.10 -m venv .env
```

Then activate the environment:

On bash:

```bash
source .env/Scripts/activate
```

On Powershell:

```pwsh
.env/Scripts/Activate.ps1
```

On cmd:

```cmd
.env/Scripts/activate.bat
```

Afterwards, the required packages can be installed by

```bash
pip install .
```

Then an auralization can be run from this directory with:

```bash
pynamic-auralization --config examples/wedge-mono-tri_wave-co1.ini
```
### For developers

`Pynamic-Pigeon-Auralization` uses the [`hatch`](https://hatch.pypa.io/dev/) build system.
For developing this application, it is recommended to [install `hatch`](https://hatch.pypa.io/dev/install/).

Using `hatch`, all requirements will be installed automatically and an auralization can be created using:

```bash
hatch run pynamic-auralization --config examples/wedge-mono-tri_wave-co1.ini
```

## Creating an auralization from scratch

When creating a new auralization, one would start by creating a new Blender project.
This project must include:

- A camera object, which is used to render the animation,
- A collection called *exactly* `Collection`, grouping the objects together,
- An object called *exactly* `Source`, defining the position of the source,
- An object called *exactly* `Receiver`, defining the position of the receiver,
- An object called *exactly* `Geometry`, defining the geometry of the scene.

Source, receiver and camera can be animated within Blender.
This can also be an animation along a curve. 
Set the 3D Viewport to the (default) `Object Mode` and save it before running the simulation.

Following this, it is recommended, that a config `ini` is created.
This defines CLI parameters and makes it easier to call the script.
An example could look like this:

```ini
blend-file=scene.blend                # Blender scene file
va-core-config=VACore_config.ini      # VACore config file to use
source-signal=signal.wav              # the signal for the source
renderer-name=MyBinauralOutdoorUrban  # name of the utilised VA renderer
```

Also reference the examples.

[^1]: The requirement for a fixed Python version is because the `bpy` package requires this.

## Options and Subroutines

Pynamic-Pigeon-Auralization streamlines the workflow of various steps in the auralization process. 
These can be configured or omitted with arguments. 
For example, you can limit the frames to be processed by passing the start frame (e.g. 20) and end frame (e.g. 40) and disable visualization using:

```bash
hatch run pynamic-auralization --config examples/wedge-mono-tri_wave-co1.ini -F 20 40 -V
```
For more information on usage and options, run:
```bash
hatch run pynamic-auralization --help
```

### Omit subroutines

The steps that are carried out can be visualized as follows. 
The arguments for deactivation are also shown.

```mermaid
flowchart LR
    A((Geometry)) --> B[Pigeon]
    B -->|Paths| C[MATLAB path generator]
    C -->|Filter| D[MATLAB/VA auralization]
    B -->|Visuals| E[Blender]
    E -->|Frames| F[ffmpeg video]
    D -->G{ffmpeg aura.+vis.}
    F -->G

    subgraph -P
    B
    end

    subgraph -A
    C
    D
    end

    subgraph -V
    E
    F
    end

    subgraph -C
    G
    end
```

`-P` Disable path simulation, also disables visualization

`-A` Disable auralization

`-V` Disable visualization

`-C` Disable combination of visualization and auralization

## Plotting the auralization parameters

This repository also contains a script to plot the auralization parameters.
This script can be run using:

```bash
pynamic-plot SUBCOMMAND OUTPUT_FOLDER
```

Or using `hatch`:

```bash
hatch run pynamic-plot SUBCOMMAND OUTPUT_FOLDER
```

For example:
```bash
hatch run pynamic-plot plot pigeon_2024-07-05T151710
```

Here, `OUTPUT_FOLDER` is the folder containing the auralization results created using `pynamic-auralization`, so make sure to replace the folder in the example accordingly.

There are two choices for the plots that can be created via `SUBCOMMAND`.
Firstly, `plot` can generate a plot for each parameter.
These can be stored in various file formats.
By default, the plots are stored in the `OUTPUT_FOLDER` as `svg` files.
The second option is `animate`, where all propagation parameters are animated over time in a single video, including a spectrogram of the final auralization and the visualization from `pynamic-auralization`.

Also note, that `pynamic-plot` includes a `--help` option for more information on the available options.
