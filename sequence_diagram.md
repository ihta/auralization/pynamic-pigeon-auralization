# Sequence Diagram Pynamic
```mermaid

sequenceDiagram
    participant A as Working Directory
    participant B as main_cli.py
    participant C as read_blend_file.py
    participant D as blender_helpers.py
    participant E as pigeon_project.py
    participant F as interpolate.oy
    participant G as path_color.py
    participant H as pigeon.exe
    participant I as prepare_visualitzation.py
    participant J as run_auralization.py
    participant K as run_auralization.m
    participant L as render_animation.py
    participant M as spectogram_animation.py


    A -->> B: hatch run pynamic-auralization
    B ->> A: material_database (empty folder)
    B ->> A: directivity_database (empty folder)
    B ->> C: read_blend_file()
    C ->> D: ensure_object_mode()
    C ->> D: blender_2_open_gl()
    D -->> C: source_loc
    C ->> D: blender_2_open_gl()
    D -->> C: receiver_loc
    C ->> D: blender_2_quaternion()
    D -->> C: source_rot
    C ->> D: blender_2_quaternion()
    D -->> C: receiver_rot
    C ->> A: geometry.dae
    C -->> B: animation_data, fps, geometry_path
    B ->> E: __init__()
    B ->> F: interpolate_3d_curve()
    F -->> B: interpolated_source_loc
    B ->> F: interpolate_3d_curve()
    F -->> B: interpolated_receiver_loc
    B ->> F: interpolate_rotation()
    F -->> B: interpolated_source_rot
    B ->> F: interpolate_rotation()
    F -->> B: interpolated_receiver_rot
    B ->> G: get_color_list()
    G ->> G: color_array = create_colormap_array()

    loop
    G ->> G: current_color = shift_method_purelightvariation()
    end

    G ->> A: color_legend.png
    G -->> B: colorlist

    loop
    B ->> E: run()
    E ->> E: config_file = self.create_config()
    E ->> A: pigeon_config.ini
    E ->> H: subprocess.run()
    H ->> A: visualization.dae
    H ->> A: pigeon_result.json
    H ->> A: pigeon_config_stats.json
    D -->> B: path of visualization.dae
    B ->> I: prepare_visualization()
    I ->> C: clean_scene()
    loop
    C ->> C: purge_orphans()
    end
    end
    B ->> J: run_auralization()
    J ->> K: subprocess.run()
    K ->> A: pigeon_auralization_tmp.wav
    K ->> A: auralization (folder)
    K ->> A: auralization.wav
    B ->> L: render_animation()
    loop
    L ->> D: ensure_object_mode()
    L ->> A: frames/nnnn.jpg
    end
    L ->> A: visualization.mp4
    B ->> M: spectogram_animation()
    M ->> A: auralization_spec.mp4
    B ->> A: animation.mp4

```